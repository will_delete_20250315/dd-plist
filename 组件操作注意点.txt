本文件是为准备在组件执行时会用到的测试文件用，通过修改以下命令行并执行即可导入文件。
修改点：
  No1:把所有【absolute_path】替换为本地绝对路径
hdc_std file send absolute_path\ddplist\sampleFiles\xmlForParse.plist      /data/app/el2/100/base/cn.openharmony.ddplist/haps/entry/files/
hdc_std file send absolute_path\ddplist\sampleFiles\xmlArrayForParse.plist /data/app/el2/100/base/cn.openharmony.ddplist/haps/entry/files/
hdc_std file send absolute_path\ddplist\sampleFiles\asciiForParse.plist    /data/app/el2/100/base/cn.openharmony.ddplist/haps/entry/files/
hdc_std file send absolute_path\ddplist\sampleFiles\binaryForParse.plist   /data/app/el2/100/base/cn.openharmony.ddplist/haps/entry/files/
hdc_std file send absolute_path\ddplist\sampleFiles\People.plist           /data/app/el2/100/base/cn.openharmony.ddplist/haps/entry/files/
  No2:上传文件后，文件owner属性为root，需修改为与files相同的owner
chown 20010054:20010054 xmlForParse.plist xmlArrayForParse.plist asciiForParse.plist binaryForParse.plist People.plist
