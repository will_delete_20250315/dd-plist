# dd-plist

## 简介
> 解析生成属性列表文件的工具库

![动画](动画_zh.gif)

## 下载安装
```shell
ohpm install @ohos/dd-plist
```
OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

1.在page页面中引入属性列表库必要的文件

```
import PropertyListParser from '../plist/PropertyListParser.ets';
```

2.使用

2.1 定义GlobalContext.ts
```
  export class GlobalContext {
      private constructor() {}
      private static instance: GlobalContext;
      private _objects = new Map<string, Object>();
      public static getContext(): GlobalContext {
          if (!GlobalContext.instance) {
            GlobalContext.instance = new GlobalContext();
          }
          return GlobalContext.instance;
      }

      getValue(value: string): Object {
         let result = this._objects.get(value);
         if (!result) {
             throw new Error("this value undefined")
         }
             return result;
         }
      setValue(key: string, objectClass: Object): void {
        this._objects.set(key, objectClass);
      }
}

 2 在src/main/ets/entryability/EntryAbility.ts初始化GlobalContext
    if(this.context.filesDir.endsWith('/')){
            GlobalContext.getContext().setValue("path", this.context.filesDir);
        }else{
            GlobalContext.getContext().setValue("path", this.context.filesDir+'/');
    }
```

2.2 在src/main/ets/entryability/EntryAbility.ts初始化GlobalContext
```
    if(this.context.filesDir.endsWith('/')){
            GlobalContext.getContext().setValue("path", this.context.filesDir);
        }else{
            GlobalContext.getContext().setValue("path", this.context.filesDir+'/');
    }
```

2.3 解析文件属性列表文件
```
     let filePath = GlobalContext.getContext().getValue("path") + this.xmlArrayFile;
        let buf = new ArrayBuffer(8192);
        // 以同步方法从文件读取数据。
        try {
            stream = fs.createStreamSync(filePath, "r+")
        } catch (err) {
            prompt.showToast({
                message: 'No file found!',
                duration: 1000
            })
        }
        if (!stream) {
            return
        }
        let nread = stream.readSync(buf)
        let temp = new Int8Array(buf);
        buf = temp.subarray(0, nread).buffer;
        let arr: Int8Array = ArrayUtils.uint8Arr2Int8Arr(new Uint8Array(buf))
        PropertyListParser.parseByInt8Array(arr, (obj: NSObject) => {
            if (obj instanceof NSArray) {
                let nsa: NSArray = obj
                this.clear()
                this.parsePlistText = "IntegerValue = " + nsa.getArray().toString()
            }
        })
```

2.4 转换为XML格式字符串
```
    let root: NSDictionary = new NSDictionary();
    let people: NSArray = new NSArray(2);
    let person1: NSDictionary = new NSDictionary();
    person1.putByObject("Name", "Peter"); //This will become a NSString
    let date = new Date()
    date.setFullYear(2011, 1, 13)
    date.setHours(9, 28)
    person1.putByObject("RegistrationDate", new NSDate(null, null, null, null, date)); //This will become a NSDate
    person1.putByObject("Age", 23); //This will become a NSNumber

    let person2: NSDictionary = new NSDictionary();
    person2.putByObject("Name", "Lisa");
    person2.putByObject("Age", 42);
    person2.putByObject("RegistrationDate", new NSDate(null, null, null, "2010-09-23T12:32:42Z"));

    people.setValue(0, person1);
    people.setValue(1, person2);

    root.put("People", people);

    this.parsePlistText = root.toXMLPropertyList()


```

2.5 生成XML格式文件
```
    let path = GlobalContext.getContext().getValue("path")+'People.plist'
    fs.createStream(path, "w+",function(err, stream){
      if (err != null) {
        console.log("e:" + err)
      } else {
        PropertyListParser.saveAsXMLByStream(root, stream)
        stream.closeSync();
      }
    });
```

## 接口说明
1. 通过文件对象解析plist文件
   `PropertyListParser.parseByFile(file: fs.File, func: Function)`
2. 通过文件路径解析plist文件
   `PropertyListParser.parse(filePath: string, func?: Function): NSObject`
3. 通过字节数组解析plist文件
   `PropertyListParser.parseByBytes(bytes, func: Function)`
4. 通过文件流解析plist文件
   `PropertyListParser.parseByStream(input: any, func?: Function): NSObject`
5. 通过NSObject对象生成一个XML格式的plist文件
   `PropertyListParser.saveAsXML(root: NSObject, file: fs.File)`
6. 通过NSObject对象生成一个XML格式的plist文件流
   `PropertyListParser.saveAsXMLToStream(root: NSObject, stream: fs.Stream)`
7. 将一个其它格式的plist文件转换为一个XML格式的plist文件
   `PropertyListParser.convertToXml(streamIn :fs.Stream, streamOut :fs.Stream)`
8. 通过file保存xml
   `saveAsXMLByFile(root: NSObject, file: fs.File)`
9. 通过字节数组解析plist文件
   `parseByInt8Array(bytes: Int8Array, func: Function)`

## 约束与限制

在下述版本验证通过：

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)
- DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)

## 目录结构
````
/dd-plist  
├── entry     # 示例代码文件夹
├── library  # 库文件夹
│   └── src/main/ets/components
│       └── plist  # 存放解析库的业务逻辑代码
│       └── utils  # 解析库所用到的工具类
│   └── index.ets  # 对外接口
├── README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/dd-plist/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/dd-plist/pulls) 共建。

## 开源协议
本项目基于 [MIT License](https://gitee.com/openharmony-sig/dd-plist/blob/master/LICENSE) ，请自由地享受和参与开源。