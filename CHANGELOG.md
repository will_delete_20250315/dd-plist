## v2.0.1
- 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)上验证通过

## v2.0.0

- 适配DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)
- ArkTS新语法适配

## v1.0.3

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）
- 修复一些异常场景导致的crash

## v1.0.2

- api8升级到api9，并转换为stage模型

## v1.0.0

- 已实现功能

1. 解析XML格式的属性列表文件
2. 解析二进制的属性列表文件
3. 解析ASCII码的属性列表文件
4. 转换为XML格式的字符串
5. 生成XML文件

- 遗留问题
1. 反序列化
   Java源码支持从属性列表对象反序列化为本机 java数据结构，因为ArkTs中的对象与Java中数据结构不同，
   在ArkTs中暂不支持转换为Java数据结构